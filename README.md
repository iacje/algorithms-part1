Coursera Algorithms, Part I: https://www.coursera.org/learn/algorithms-part1/

Percolation Assignment on Union-Find: 
http://coursera.cs.princeton.edu/algs4/assignments/percolation.html

Deques and Randomized Queues Assignment on Stacks and Queues:
http://coursera.cs.princeton.edu/algs4/assignments/queues.html

Pattern Recognition Assignment on MergeSort and QuickSort:
http://coursera.cs.princeton.edu/algs4/assignments/collinear.html

8 Puzzle Assignment on Priority Queues:
http://coursera.cs.princeton.edu/algs4/checklists/8puzzle.html

Kd-Trees Assignment on BSTs:
http://coursera.cs.princeton.edu/algs4/assignments/kdtree.html
