import java.util.Stack;
import java.util.Arrays;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.In;

public class BruteCollinearPoints {
    // stack of line segments
    private final Stack<LineSegment> st = new Stack<>();
    
    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] points) {
        if (points == null) throw new IllegalArgumentException();
        int n = points.length;
        for (Point p : points) {
            if (p == null) throw new IllegalArgumentException();
        }
        Point[] sortedP = Arrays.copyOf(points, n);
        Arrays.sort(sortedP);
        for (int i = 1; i < n; i++) {
            if (sortedP[i].compareTo(sortedP[i-1]) == 0) {
                throw new IllegalArgumentException();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                for (int k = j + 1; k < n; k++) {
                    for (int m = k + 1; m < n; m++) {
                        if (sortedP[i].slopeTo(sortedP[j]) == 
                            sortedP[i].slopeTo(sortedP[k]) &&
                            sortedP[i].slopeTo(sortedP[j]) == 
                            sortedP[i].slopeTo(sortedP[m])) {
                            st.push(new LineSegment(sortedP[i], sortedP[m]));
                        }
                    }
                }
            }
        }
    }

    // the number of line segments
    public int numberOfSegments() { return st.size(); }
    
    // the line segments
    public LineSegment[] segments() {
        LineSegment[] segs = new LineSegment[numberOfSegments()];
        int i = 0;
        for (LineSegment line : st) {
            segs[i++] = line;
        }
        return Arrays.copyOf(segs, numberOfSegments());
    }
    
    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }
        
        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            StdDraw.setPenRadius(0.025);
            StdDraw.setPenColor(StdDraw.BLUE);
            p.draw();
        }
        StdDraw.show();
        
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            StdDraw.setPenRadius(0.005);
            StdDraw.setPenColor(StdDraw.RED);
            segment.draw();
        }
        StdDraw.show();
    }
}