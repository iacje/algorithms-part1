import java.util.Stack;
import java.util.Arrays;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.In;

public class FastCollinearPoints {
    // stack of line segments
    private final Stack<LineSegment> st = new Stack<>();
    
    // finds all line segments containing 4 or more points
    public FastCollinearPoints(Point[] points) {
        if (points == null) throw new IllegalArgumentException();
        int n = points.length;
        for (Point p : points) {
            if (p == null) throw new IllegalArgumentException();
        }
        Point[] sortedP = Arrays.copyOf(points, n);
        Arrays.sort(sortedP);
        for (int i = 1; i < n; i++) {
            if (sortedP[i].compareTo(sortedP[i-1]) == 0) {
                throw new IllegalArgumentException();
            }
        }
        for (int i = 0; i < n; i++) {
            Arrays.sort(sortedP);
            Point anchor = points[i];
            Arrays.sort(sortedP, anchor.slopeOrder());
            Point start = sortedP[0];
            double currentSlope = anchor.slopeTo(start);
            int count = 1;
            for (int j = 1; j < n; j++) {
                double newSlope = anchor.slopeTo(sortedP[j]);
                if (newSlope == currentSlope) {
                    count++;
                    // reached end of array
                    if (count >= 3 && anchor.compareTo(start) < 0 && j == n-1) {
                        st.push(new LineSegment(anchor, sortedP[j]));
                        // reset
                        start = sortedP[j];
                        currentSlope = newSlope;
                        count = 1;
                    }
                }
                else { // slope changed
                    // anchor is start of segment in terms of natural order
                    if (count >= 3 && anchor.compareTo(start) < 0) {
                        st.push(new LineSegment(anchor, sortedP[j-1]));
                    }
                    // reset
                    start = sortedP[j];
                    currentSlope = newSlope;
                    count = 1;
                }
            }
        }
    }
        
    // the number of line segments
    public int numberOfSegments() { return st.size(); }
    
    // the line segments
    public LineSegment[] segments() {
        LineSegment[] segs = new LineSegment[numberOfSegments()];
        int i = 0;
        for (LineSegment line : st) {
            segs[i++] = line;
        }
        return Arrays.copyOf(segs, numberOfSegments());
    }
    
    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }
        
        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            StdDraw.setPenRadius(0.01);
            StdDraw.setPenColor(StdDraw.BLUE);
            p.draw();
        }
        StdDraw.show();
        
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            StdDraw.setPenRadius(0.005);
            StdDraw.setPenColor(StdDraw.RED);
            segment.draw();
        }
        StdDraw.show();
    }
}