import edu.princeton.cs.algs4.StdOut;
import java.util.NoSuchElementException;
import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {
    private Node first; // beginning of queue
    private Node last; // end of queue
    private int n; // number of elements on queue

    private class Node {
        private Item item;
        private Node prev;
        private Node next;
    }
    
    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        n = 0;  
    }
    
   // is the deque empty?
    public boolean isEmpty() {
        return first == null;
    }
    
    // return the number of items on the deque
    public int size() {
        return n;
    }
    
    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) throw new IllegalArgumentException();
        Node newFirst = new Node();
        newFirst.item = item;
        if (isEmpty()) {
            first = newFirst;
            last = first;
        } else {
            newFirst.next = first;
            first.prev = newFirst;
            first = newFirst;
        }
        n++;    
    }
    
    // add the item to the end
    public void addLast(Item item) {
        if (item == null) throw new IllegalArgumentException();
        Node newLast = new Node();
        newLast.item = item;
        if (isEmpty()) {
            last = newLast;
            first = newLast;
        } else {
            newLast.prev = last;
            last.next = newLast;
            last = newLast;
        }
        n++;
    }
    
    // remove and return the item from the front
    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException("Deque underflow");
        Item item = first.item;
        first = first.next;
        n--;
        if (isEmpty()) {
            last = null;
        } else {
            first.prev = null;
        }
        return item;
    }
    
    // remove and return the item from the end
    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException("Deque underflow");
        Item item = last.item;
        last = last.prev;
        n--;
        if (last == null) {
            first = null;
        } else {
            last.next = null;
        }
        return item;
    }
    
    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new ListIterator();
    }
    
    private class ListIterator implements Iterator<Item> {
        private Node current = first;
        
        public boolean hasNext() { return current != null; }
        
        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }
        
        public void remove() { throw new UnsupportedOperationException(); }   
    }
    
    // unit testing (optional)
    public static void main(String[] args) {
        Deque<Integer> deque = new Deque<>();
        for (int i = 1; i < 11; i++) {
            deque.addFirst(i);
        }
        for (Integer i : deque) {
            StdOut.print(i);
        }
        for (int i = 1; i < 11; i++) {
            StdOut.print(deque.removeLast());
        }
        
        Deque<String> strDeque = new Deque<>();
        StdOut.print(strDeque.removeFirst());
    }
}