import java.util.NoSuchElementException;
import java.util.Iterator;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdOut;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] q;
    private int n; // number of elements on queue
    
    // construct an empty randomized queue
    public RandomizedQueue() {
        q = (Item[]) new Object[2];
        n = 0;
    }
    
    // is the randomized queue empty? 
    public boolean isEmpty() { return n == 0; }
    
   // return the number of items on the randomized queue
    public int size() { return n; }
   
    private void resize(int capacity) {
        Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < n; i++) {
            temp[i] = q[i];
        }
        q = temp;
    }
    
    // add the item 
    public void enqueue(Item item) {
        if (item == null) throw new IllegalArgumentException();
        if (n == q.length) resize(2 * q.length);
        q[n++] = item;
    }
    
    // remove and return a random item
    public Item dequeue() {
        if (isEmpty()) throw new NoSuchElementException();
        int randIndex = StdRandom.uniform(0, n);
        Item item = q[randIndex];
        n--;
        if (!isEmpty()) {
            q[randIndex] = q[n];
            q[n] = null;
        } else {
            q[randIndex] = null;
        }
        if (n > 0 && n == q.length/4) resize(q.length/2);
        return item;
    }
   
    // return a random item (but do not remove it)
    public Item sample() {
        if (isEmpty()) throw new NoSuchElementException();
        return q[StdRandom.uniform(0, n)];
    }
    
    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new ArrayIterator();
    }
    
    private class ArrayIterator implements Iterator<Item> {
        // Rearranges elements of the array in uniformly random order
        
        private final Item[] shuffled;
        private int i = 0;
        
        public ArrayIterator() {
            Item[] temp = (Item[]) new Object[n];
            for (int j = 0; j < n; j++) {
                temp[j] = q[j];
            }
            StdRandom.shuffle(temp);
            shuffled = temp;
        }
        
        public boolean hasNext() { return i < n; }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = shuffled[i++];
            return item;
        }
        
        public void remove() { throw new UnsupportedOperationException(); }
    }
    
    // unit testing (optional)
    public static void main(String[] args) {
        RandomizedQueue<String> rQueue = new RandomizedQueue<>();
        for (String s : rQueue) {
            StdOut.println(s);
        }
        rQueue.enqueue("hi");
        rQueue.enqueue("Hello");
        StdOut.println("Size: " + rQueue.size());
        for (String s : rQueue) {
            StdOut.println(s);
        }
        StdOut.println(rQueue.sample());
        StdOut.println("Size: " + rQueue.size());
        StdOut.println(rQueue.dequeue());
        StdOut.println("Size: " + rQueue.size());
        StdOut.println(rQueue.dequeue());
        StdOut.println("Size: " + rQueue.size());
    }
}