import java.util.Stack;

public class Board {
    private final int n;
    private final int[][] grid;
    private int zeroRow;
    private int zeroCol;
    
    // construct a grid from an n-by-n array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        n = blocks[0].length;
        grid = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                grid[i][j] = blocks[i][j];
                if (grid[i][j] == 0) {
                    zeroRow = i;
                    zeroCol = j;
                }
            }
        }
    }
    
    // grid dimension n
    public int dimension() { return n; }
    
    // number of blocks out of place
    public int hamming() {
        int sum = 0;
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 0 && grid[i][j] != counter) sum++;
                counter++;
            }
        }
        return sum;
    }
    
    private int getRow(int block) { return (block - 1) / n; }
    
    private int getCol(int block) { return (block - 1) % n; }
    
    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 0) {
                    sum += Math.abs(i - getRow(grid[i][j])) + 
                    Math.abs(j - getCol(grid[i][j]));
                }
            }
        }
        return sum;
    }
    
    // is this grid the goal grid?
    public boolean isGoal() {
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != 0 && grid[i][j] != counter) return false;
                counter++;
            }
        }
        return true;
    }
    
        
    // a grid that is obtained by exchanging any pair of blocks
    public Board twin() {
        int flipX = 0;
        int flipY = 0;
        int flopX = 0;
        int flopY = 0;
        boolean hasFlip = false;
        boolean hasFlop = false;
        
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                blocks[i][j] = grid[i][j];
                if (!hasFlip && blocks[i][j] != 0) {
                    flipX = i;
                    flipY = j;
                    hasFlip = true;
                }
                else if (!hasFlop && blocks[i][j] != 0) {
                    flopX = i;
                    flopY = j;
                    hasFlop = true;
                }
            }
        }
        blocks[flipX][flipY] = grid[flopX][flopY];
        blocks[flopX][flopY] = grid[flipX][flipY];
        return new Board(blocks);
    }
    
     // does this grid equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board that = (Board) y;
        if (this.grid.length != that.grid.length) return false;
        if (this.grid[0].length != that.grid[0].length) return false;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (this.grid[i][j] != that.grid[i][j]) return false;
            }
        }
        return true;
    }           
    
   private boolean isValidIndex(int row, int col) {
       return row >= 0 && row < n && col >= 0 && col < n;
   }
   
   private void swap(int[][] b, int row1, int col1, int row2, int col2) {
       int temp = b[row1][col1];
       b[row1][col1] = b[row2][col2];
       b[row2][col2] = temp;
   }
   
   private int[][] copy() {
       int[][] myCopy = new int[n][n];
       for (int i = 0; i < n; i++) {
           for (int j = 0; j < n; j++) {
               myCopy[i][j] = grid[i][j];
           }
       }
       return myCopy;
   }
   
    // all neighboring grids
    public Iterable<Board> neighbors() {
        Stack<Board> st = new Stack<>();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (Math.abs(i + j) == 1 && 
                    isValidIndex(zeroRow + i, zeroCol + j)) {
                    int[][] neighbor = copy();
                    swap(neighbor, zeroRow, zeroCol, zeroRow + i, zeroCol + j);
                    st.push(new Board(neighbor));
                }
            }
        }
        return st;
    }
        
    // string representation of this grid (in the output format specified below)
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(n + "\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s.append(String.format("%2d ", grid[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }
    
    // unit tests (not graded)
    public static void main(String[] args) {
        int[][] temp = {{8, 1, 3}, {4, 2, 0}, {7, 6, 5}};
        Board myBoard = new Board(temp);
        System.out.println(myBoard.hamming());
        System.out.println(myBoard.manhattan());
        int[][] temp2 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 0}};
        Board myBoard2 = new Board(temp2);
        System.out.println(myBoard2.isGoal());
        System.out.println("twin: " + myBoard.twin());
        for (Board b : myBoard.neighbors()) System.out.println(b);
    
    } 
}