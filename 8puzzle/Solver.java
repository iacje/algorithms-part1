import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Solver {
    private final SearchNode last;
    
    private class SearchNode implements Comparable<SearchNode> {
        private final Board board;
        private final int numMoves;
        private final int manhattanDist;
        private final SearchNode prevNode;
        
        public SearchNode(Board board, int numMoves, SearchNode prev) {
            if (board == null) throw new IllegalArgumentException();
            this.board = board;
            this.numMoves = numMoves;
            this.manhattanDist = board.manhattan();
            this.prevNode = prev;
        }
        
        public int compareTo(SearchNode that) {
            int diff = this.manhattanDist + this.numMoves - 
                (that.manhattanDist + that.numMoves);
            if (diff < 0) return -1;
            else if (diff > 0) return 1;
            else {
                diff = this.manhattanDist - that.manhattanDist;
                if (diff < 0) return -1;
                else if (diff > 0) return 1;
                return 0;
            }
        }
    }
    
    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        MinPQ<SearchNode> pq = new MinPQ<>();
        MinPQ<SearchNode> pqTwin = new MinPQ<>();
        pq.insert(new SearchNode(initial, 0, null));
        pqTwin.insert(new SearchNode(initial.twin(), 0, null));
        SearchNode current = pq.min();
        while (!current.board.isGoal() && !pqTwin.min().board.isGoal()) {
            SearchNode node = pq.delMin();
            Iterable<Board> neighbors = node.board.neighbors();
            for (Board b : neighbors) {
                if (node.prevNode == null || !b.equals(node.prevNode.board)) {
                    pq.insert(new SearchNode(b, node.numMoves + 1, node));
                }
            }
            current = pq.min();
            
            node = pqTwin.delMin();
            neighbors = node.board.neighbors();
            for (Board b : neighbors) {
                if (node.prevNode == null || !b.equals(node.prevNode.board)) {
                    pqTwin.insert(new SearchNode(b, node.numMoves + 1, node));
                }
            }
        }
        last = current;
    }
    
    // is the initial board solvable?
    public boolean isSolvable()  {
        return last.board.isGoal();
    }
    
    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        if (!isSolvable()) return -1;
        return last.numMoves;
    }
    
    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!isSolvable()) return null;
        Stack<Board> st = new Stack<>();
        SearchNode temp = last;
        while (temp != null) {
            st.push(temp.board);
            temp = temp.prevNode;
        }
        return st;
    }
    
    // solve a slider puzzle (given below)
    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);
        
        // solve the puzzle
        Solver solver = new Solver(initial);
        
        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}