import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdOut;

public class PercolationStats {
   private static final double CONFIDENCE_95 = 1.96;
   private final double[] estimates;
   private Double mean;
   private Double sd;
   
   // perform trials independent experiments on an n-by-n grid
   public PercolationStats(int n, int trials)  {
       if (n <= 0 || trials <= 0) throw new IllegalArgumentException();
       estimates = new double[trials];
       for (int i = 0; i < trials; i++) {
           Percolation perc = new Percolation(n);
           while (!perc.percolates()) {
               int row = StdRandom.uniform(1, n + 1);
               int col = StdRandom.uniform(1, n + 1);
               while (perc.isOpen(row, col)) {
                   row = StdRandom.uniform(1, n + 1);
                   col = StdRandom.uniform(1, n + 1);
               }
               perc.open(row, col);
           }
           estimates[i] = (double) perc.numberOfOpenSites() / (n * n);
       }
   } 
   
   // sample mean of percolation threshold
   public double mean() {
       if (mean == null) mean = StdStats.mean(estimates);
       return mean;
   }
   
   // sample standard deviation of percolation threshold
   public double stddev() {
       if (sd == null) sd = StdStats.stddev(estimates);
       return sd;
   }
   
   // low  endpoint of 95% confidence interval
   public double confidenceLo() {
       if (mean == null) mean = StdStats.mean(estimates);
       if (sd == null) sd = StdStats.stddev(estimates);
       return mean - (CONFIDENCE_95 * sd / Math.sqrt(estimates.length));
   }
   
   // high endpoint of 95% confidence interval
   public double confidenceHi() {
       if (mean == null) mean = StdStats.mean(estimates);
       if (sd == null) sd = StdStats.stddev(estimates);
       return mean + (CONFIDENCE_95 * sd / Math.sqrt(estimates.length));
   }                 

   // test client (described below)
   public static void main(String[] args) {
       int n = Integer.parseInt(args[0]);
       int trials = Integer.parseInt(args[1]);
       PercolationStats stats = new PercolationStats(n, trials);
       StdOut.println("mean = " + stats.mean());
       StdOut.println("stddev = " + stats.stddev());
       StdOut.println("95% confidence interval = [" + stats.confidenceLo() + ", " 
                          + stats.confidenceHi() + "]");
   }       
}