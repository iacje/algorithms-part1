import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
   private final int n;
   private int numOpenSites;
   private boolean[] grid;
   private final WeightedQuickUnionUF uf;
   private final WeightedQuickUnionUF ufTop;
   
   // create n-by-n grid, with all sites blocked
   public Percolation(int n) {
       if (n <= 0) throw new IllegalArgumentException();
       this.n = n;
       grid = new boolean[n * n];
       // index n for top site, index n+1 for bottom site
       uf = new WeightedQuickUnionUF(n*n + 2);
       ufTop = new WeightedQuickUnionUF(n*n + 2); 
   }
   
   // map 2D coordinates to 1D coordinates
   private int xyTo1D(int row, int col) {
       return row * n + col;
   }
   
   private boolean invalidIndex(int row, int col) {
       return row <= 0 || row > n || col <= 0 || col > n;
   }
   
   // open site (row, col) if it is not open already
   public void open(int row, int col) {
       if (invalidIndex(row, col)) {
           throw new IllegalArgumentException("index out of bounds");
       }
       if (!isOpen(row, col)) {
           grid[xyTo1D(row-1, col-1)] = true;
           numOpenSites++;
           if (row == 1) {
               uf.union(xyTo1D(row-1, col-1), n*n);
               ufTop.union(xyTo1D(row-1, col-1), n*n);
           }
           if (row == n) {
               uf.union(xyTo1D(row-1, col-1), n*n+1);
           }
       }
       for (int i = -1; i <= 1; i++) {
           for (int j = -1; j <= 1; j++) {
               if (Math.abs(i + j) == 1 && !invalidIndex(row+i, col+j) && 
                   isOpen(row+i, col+j)) {
                   ufTop.union(xyTo1D(row-1, col-1), xyTo1D(row-1+i, col-1+j));
                   uf.union(xyTo1D(row-1, col-1), xyTo1D(row-1+i, col-1+j));
               }
           }
       }
   }
   
   // is site (row, col) open?
   public boolean isOpen(int row, int col) {
       if (invalidIndex(row, col)) {
           throw new IllegalArgumentException("index out of bounds");
       }
       return grid[xyTo1D(row-1, col-1)];
   }
   
   // is site (row, col) full?
   public boolean isFull(int row, int col)  {
       return isOpen(row, col) && ufTop.connected(xyTo1D(row-1, col-1), n*n); 
   }
   
   // number of open sites
   public int numberOfOpenSites() {
       return numOpenSites;
   }
       
   // does the system percolate?
   public boolean percolates() {
       return uf.connected(n*n, n*n+1);
   }

    // test client (optional)
   public static void main(String[] args) {
//       Percolation percolation = new Percolation(2);
//       percolation.open(1, 1);
//       percolation.open(1, 2);
//       System.out.println("Open Sites:");
//       System.out.println(percolation.isOpen(1, 1));
//       System.out.println(percolation.isOpen(1, 2));
//       System.out.println(percolation.isOpen(2, 1));
//       System.out.println(percolation.isOpen(2, 2));
//       System.out.println("Connected:");
//       System.out.println(percolation.uf.connected(0, 1));
//       System.out.println(percolation.uf.connected(2, 1));
//       System.out.println("Percolates:" + percolation.percolates());
   }
}