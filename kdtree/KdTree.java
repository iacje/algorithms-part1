import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.Stack;

public class KdTree {
    
    private static final boolean X = true;
    private Node root;
    private int n;
    private double champDist;

    private static class Node {
        private final Point2D p;      // the point
        private final RectHV rect;    // the axis-aligned rectangle corresponding to this node
        private Node lb;        // the left/bottom subtree
        private Node rt;        // the right/top subtree
        
        public Node(Point2D p, RectHV rect) {
            this.p = p;
            this.rect = rect;
        }
    }
    
    // construct an empty set of points
    public KdTree() { }
    
    // is the set empty? 
    public boolean isEmpty() { return n == 0; }
    
    // number of points in the set
    public int size() { return n; }
    
    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        root = insert(root, p, null, X);
    }
    
    // coursera.org/
    // learn/algorithms-part1/discussions/weeks/5/threads/T-r2NsedEeaFyhIY_gUU0g
    private Node insert(Node curr, Point2D p, Node prev, boolean split) {
        if (curr == null) {
            n++;
            // tree empty
            if (prev == null) return new Node(p, new RectHV(0, 0, 1, 1));
            
            // split previous node's rectangle
            RectHV r;
            double cmp = (split != X) ? p.x() - prev.p.x() : p.y() - prev.p.y();
            if (split != X) {
                if (cmp < 0) r = new RectHV(prev.rect.xmin(), prev.rect.ymin(), 
                                             prev.p.x(), prev.rect.ymax());
                else r = new RectHV(prev.p.x(), prev.rect.ymin(), 
                                     prev.rect.xmax(), prev.rect.ymax());
            } else {
                if (cmp < 0) r = new RectHV(prev.rect.xmin(), prev.rect.ymin(),
                                             prev.rect.xmax(), prev.p.y());
                else r = new RectHV(prev.rect.xmin(), prev.p.y(), 
                                     prev.rect.xmax(), prev.rect.ymax());
            }
            return new Node(p, r);
        }
        if (curr.p.equals(p)) return curr;
        double cmp = (split == X) ? p.x() - curr.p.x() : p.y() - curr.p.y();
        if (cmp < 0) curr.lb = insert(curr.lb, p, curr, !split);
        else curr.rt = insert(curr.rt, p, curr, !split);
        return curr;
    }
    
    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        return get(p) != null;
    }
    
    private Node get(Point2D p) { return get(root, p, X); }
        
    private Node get(Node curr, Point2D p, boolean split) {
        if (curr == null) return null;
        if (curr.p.equals(p)) return curr;
        double cmp = (split == X) ? p.x() - curr.p.x() : p.y() - curr.p.y();
        if (cmp < 0) return get(curr.lb, p, !split);
        return get(curr.rt, p, !split);
    }
    
    // draw all points to standard draw
    public void draw() { 
        StdDraw.enableDoubleBuffering();
        draw(root, X);
        StdDraw.show();
    }
    
    private void draw(Node curr, boolean split) {
        if (curr == null) return;
        StdDraw.setXscale(0, 1);
        StdDraw.setYscale(0, 1);
        StdDraw.setPenRadius(0.01);
        StdDraw.setPenColor(StdDraw.BLACK);
        curr.p.draw();
        if (split == X) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(curr.p.x(), curr.rect.ymin(), 
                         curr.p.x(), curr.rect.ymax());
        } else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(curr.rect.xmin(), curr.p.y(),
                         curr.rect.xmax(), curr.p.y());
        }
        draw(curr.lb, !split);
        draw(curr.rt, !split);
    }
    
    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new IllegalArgumentException();
        Stack<Point2D> st = new Stack<>();
        range(root, rect, st);
        return st;
    }
    
    private void range(Node curr, RectHV rect, Stack<Point2D> st) {
        if (curr == null || !rect.intersects(curr.rect)) return;
        if (rect.contains(curr.p)) st.push(curr.p);
        range(curr.lb, rect, st);
        range(curr.rt, rect, st);
    }
    
    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        if (root == null) return null;
        champDist = Double.POSITIVE_INFINITY;
        return nearest(root, p, root.p, X);
    }
    
    // champ represents the current neareast neighbor
    private Point2D nearest(Node curr, Point2D p, Point2D champ, boolean split) {
        if (curr == null) return champ;
        if (champDist <= curr.rect.distanceSquaredTo(p)) return champ;
        double dist = curr.p.distanceSquaredTo(p);
        if (dist < champDist) {
            champ = curr.p;
            champDist = dist;
        }
        if (split == X) {
            if (p.x() < curr.p.x()) {
                champ = nearest(curr.lb, p, champ, !X);
                champ = nearest(curr.rt, p, champ, !X);
            } else {
                champ = nearest(curr.rt, p, champ, !X);
                champ = nearest(curr.lb, p, champ, !X);
            }
        } else {
            if (p.y() < curr.p.y()) {
                champ = nearest(curr.lb, p, champ, !X);
                champ = nearest(curr.rt, p, champ, !X);
            } else {
                champ = nearest(curr.rt, p, champ, !X);
                champ = nearest(curr.lb, p, champ, !X);
            }
        }
        return champ;
    }

    // unit testing of the methods (optional) 
    public static void main(String[] args) {
        KdTree tree = new KdTree();
        tree.insert(new Point2D(0.7, 0.2));
        tree.insert(new Point2D(0.5, 0.4));
        tree.insert(new Point2D(0.2, 0.3));
        tree.insert(new Point2D(0.4, 0.7));
        tree.insert(new Point2D(0.9, 0.6));
        System.out.println(tree.contains(new Point2D(0.9, 0.6)));
        System.out.println(tree.contains(new Point2D(0.7, 0.2)));
        System.out.println(tree.contains(new Point2D(0.5, 0.5)));
        for (Point2D p : tree.range(new RectHV(0, 0, 1, 1))) {
            System.out.println(p);
        }
        tree.draw();
    }
}