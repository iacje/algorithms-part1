import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdDraw;

public class PointSET {
    private final SET<Point2D> set;
    
   // construct an empty set of points
    public PointSET() {
        set = new SET<Point2D>();
    }
    
    // is the set empty? 
    public boolean isEmpty() {
        return set.isEmpty();
    }
    
    // number of points in the set
    public int size() {
        return set.size();
    }
    
    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        set.add(p);
    }
    
    // does the set contain point p? 
    public boolean contains(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        return set.contains(p);
    }
   
    // draw all points to standard draw 
    public void draw() {
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 1);
        StdDraw.setYscale(0, 1);
        StdDraw.setPenColor(StdDraw.BLACK);
        for (Point2D p : set) {
            p.draw();
        }
        StdDraw.show();
    }
    
    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new IllegalArgumentException();
        Stack<Point2D> st = new Stack<>();
        for (Point2D p : set) {
            if (rect.contains(p)) st.push(p);
        }
        return st;
    }
    
    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (size() == 0) return null;
        Point2D nneighbor = set.min();
        for (Point2D q : set) {
            if (p.distanceSquaredTo(q) < p.distanceSquaredTo(nneighbor)) nneighbor = q;
        }
        return nneighbor;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        PointSET set = new PointSET();
        set.insert(new Point2D(0.7, 0.2));
        set.insert(new Point2D(0.2, 0.3));
        set.draw();
        System.out.println(set.nearest(new Point2D(2, 0)));
    }
}